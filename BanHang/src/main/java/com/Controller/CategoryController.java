package com.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.Service.CategoryService;

import com.model.Category;



@RestController
@RequestMapping("/abc")
public class CategoryController {
	
	@Autowired
	CategoryService categoryservice;
	
	@GetMapping("/category1")
	  public List<Category> getAllCategory() {
	    return  categoryservice.getAllCategory();
	  }
	

	@GetMapping("/{id}")
	public Category findOne(@PathVariable("id") int id) {
		Category category = categoryservice.findOne(id);
		if(category==null)
		{
			System.out.println("ID"+id+"not found");
		}
	
	return category;
	}
	
	
	@PostMapping("/category")
	public Category add(@RequestBody Category category) {
		categoryservice.save(category);
		return category;
	}
	
	@GetMapping("/ping")
	public String ping() {
		return "pong";
	}
	

}
