package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name ="product_oder")
public class ProductOder {
	
	@Column(name="product_id")
	private int productId;
	

	@Column(name="oder_id")
	private int oderId;


	public int getProductId() {
		return productId;
	}


	public void setProductId(int productId) {
		this.productId = productId;
	}


	public int getOderId() {
		return oderId;
	}


	public void setOderId(int oderId) {
		this.oderId = oderId;
	}
	
	

}
