package com.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.model.Oder;

@Repository
public interface OderRepository extends JpaRepository<Oder, Integer> {

	

	Oder findOne(int id);

}
