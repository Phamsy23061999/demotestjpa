package com.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.model.Category;



@Service
public interface CategoryService {
	
	public List<Category> getAllCategory();
	
	public Category getNewById(int id);
	
	public Category findOne(int id);
	
	public void save(Category catgory);

}
