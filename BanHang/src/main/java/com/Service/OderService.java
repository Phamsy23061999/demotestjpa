package com.Service;

import java.util.List;

import org.springframework.stereotype.Service;


import com.model.Oder;

@Service
public interface OderService {

	public List<Oder> getAllCategory();
	
	public Oder getNewById(int id);
	
	public Oder findOne(int id);
	
	public void save(Oder oders);

}
