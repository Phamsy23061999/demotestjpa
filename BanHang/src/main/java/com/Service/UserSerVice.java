package com.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.model.User;

@Service
public interface UserSerVice {
	
	public List<User> getAllCategory();
	
	public User getNewById(int id);
	
	public User findOne(int id);
	
	public void save(User users);

}
