package com.Service.iml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Repository.UserRepository;
import com.Service.UserSerVice;

import com.model.User;

@Service
public class UserSVIml implements UserSerVice{
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public List<User> getAllCategory() {
		
		return (List<User>) userRepository.findAll();
	}

	@Override
	public User getNewById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findOne(int id) {
		// TODO Auto-generated method stub
		return userRepository.findOne((int)id);
	}

	@Override
	public void save(User users) {
		userRepository.save(users);
		
	}

}
