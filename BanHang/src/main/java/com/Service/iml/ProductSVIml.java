package com.Service.iml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Repository.ProductRepository;
import com.Service.ProductService;

import com.model.Product;

@Service
public class ProductSVIml implements ProductService{
	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<Product> getAllCategory() {
		
		return (List<Product>)  productRepository.findAll();
	}

	@Override
	public Product getNewById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Product findOne(int id) {
		
		return  productRepository.findOne((int) id);
	}

	@Override
	public void save(Product products) {
		productRepository.save(products);
		
	}
	

}
