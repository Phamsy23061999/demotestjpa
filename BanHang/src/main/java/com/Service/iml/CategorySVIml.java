package com.Service.iml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Repository.CategoryRepository;
import com.Service.CategoryService;

import com.model.Category;



@Service
public class CategorySVIml implements CategoryService{
	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public List<Category> getAllCategory() {
		
		return (List<Category>) categoryRepository.findAll();
	}

	

	@Override
	public Category findOne(int id) {
		
		return categoryRepository.findOne((int)id);
	}

	@Override
	public void save(Category catgory) {
		categoryRepository.save(catgory);
		
	}



	@Override
	public Category getNewById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
