package com.Service.iml;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.Repository.OderRepository;
import com.Service.OderService;

import com.model.Oder;

@Service
public class OderSVIml implements OderService{
	@Autowired
	OderRepository oderRepository;

	@Override
	public List<Oder> getAllCategory() {
	
		return (List<Oder>) oderRepository.findAll();
	}

	@Override
	public Oder getNewById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Oder findOne(int id) {
		
		return oderRepository.findOne((int) id);
	}

	@Override
	public void save(Oder oders) {
		oderRepository.save(oders);
		
	}
	

}
