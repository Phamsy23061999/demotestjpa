package com.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public interface ProductService {
	public List<Product> getAllCategory();
	
	public Product getNewById(int id);
	
	public Product findOne(int id);
	
	public void save(Product products);
}
